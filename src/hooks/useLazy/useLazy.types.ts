import { ComponentType, LazyExoticComponent, ReactNode } from 'react';

export interface LazyFragmentProps {
  children: ReactNode;
}

export type LazyHook = <Props = LazyFragmentProps>(
  fetcher: () => Promise<ComponentType<Props> | null>,
  deps?: any[]
) => LazyExoticComponent<
  ComponentType<Props> | ComponentType<LazyFragmentProps>
>;
