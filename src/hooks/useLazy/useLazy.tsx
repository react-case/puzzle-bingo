import { lazy, useMemo } from 'react';
import * as Types from './useLazy.types';

const useLazy: Types.LazyHook = (fetcher, deps = []) =>
  useMemo(
    () =>
      lazy(async () => ({
        default:
          (await fetcher()) ||
          (({ children }: Types.LazyFragmentProps) => <>{children}</>),
      })),
    deps
  );

export default useLazy;
