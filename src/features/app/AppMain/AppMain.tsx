import * as Styles from './AppMain.styles';
import * as Types from './AppMain.types';
import { AppProvider } from '~/contexts';

export default function AppMain({ children }: Types.AppMainProps) {
  return (
    <AppProvider>
      <Styles.Main maxWidth="lg" className="app" component="main">
        {children}
      </Styles.Main>
    </AppProvider>
  );
}
