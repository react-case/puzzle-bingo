import Container from '@mui/material/Container';
import { withStyles } from 'tss-react/mui';

export const Main = withStyles(
  Container,
  () => ({
    root: {
      position: 'relative',
    },
  }),
  { name: 'Main' }
);
