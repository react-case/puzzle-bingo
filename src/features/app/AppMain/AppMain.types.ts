import { ReactNode } from 'react';

export interface AppMainProps {
  children: ReactNode;
}
