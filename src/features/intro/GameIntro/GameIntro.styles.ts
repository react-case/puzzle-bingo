import Container from '@mui/material/Container';
import ListItemButton from '@mui/material/ListItemButton';
import Typography from '@mui/material/Typography';
import { withStyles } from 'tss-react/mui';

export const Main = withStyles(
  Container,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',

      '& > * + *': {
        marginTop: `${theme.spacing(3)} !important`,
      },
    },
  }),
  { name: 'Main' }
);

export const Title = withStyles(
  Typography,
  (theme) => ({
    root: {
      position: 'relative',
      marginBottom: theme.spacing(20),
      transform: 'rotate(-15deg)',
      fontWeight: 'bolder',
      textShadow: '0.1em 0.1em 0.2em black',

      '& > img': {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%) rotate(30deg)',
        zIndex: -1,
      },
    },
  }),
  { name: 'Title' }
);

export const Option = withStyles(
  ListItemButton,
  (theme) => ({
    root: {
      borderRadius: `${theme.spacing(4)} / 50%`,
      padding: theme.spacing(1, 3),
    },
  }),
  { name: 'Option' }
);

export const Status = withStyles(
  Typography,
  (theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      gap: theme.spacing(2),
    },
  }),
  {
    name: 'Status',
  }
);
