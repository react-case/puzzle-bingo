import { ReactNode } from 'react';

export type Statuses = 'NEW_GAME' | 'WAITING';

export interface LazyFragmentProps {
  children: ReactNode;
}
