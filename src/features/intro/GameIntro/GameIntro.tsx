import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import List from '@mui/material/List';
import ListItemText from '@mui/material/ListItemText';
import { useState } from 'react';

import * as Styles from './GameIntro.styles';
import * as Types from './GameIntro.types';
import { useApp } from '~/contexts/AppContext';

const statuses: Record<Types.Statuses, string> = {
  NEW_GAME: 'Creating new game...',
  WAITING: 'Waiting for other players...',
};

export default function GameIntro() {
  const { create, join, exit } = useApp();
  const [status, setStatus] = useState<Types.Statuses | null>(null);

  return (
    <Styles.Main maxWidth="sm">
      <Styles.Title variant="h2" color="secondary" align="center">
        Puzzle Bingo
        <img alt="logo" src="/logo.png" />
      </Styles.Title>

      {!status && (
        <List>
          <Styles.Option
            onClick={async () => {
              setStatus('NEW_GAME');

              await create({
                onOpen: () => setStatus('WAITING'),
              });
            }}
          >
            <ListItemText
              primaryTypographyProps={{ variant: 'h4', color: 'primary' }}
              primary="New Game"
            />
          </Styles.Option>

          <Styles.Option>
            <ListItemText
              primaryTypographyProps={{ variant: 'h4', color: 'secondary' }}
              primary="Join Game"
            />
          </Styles.Option>
        </List>
      )}

      {status && (
        <>
          <Styles.Status variant="h6" color="text.secondary">
            <CircularProgress color="info" />

            {statuses[status]}
          </Styles.Status>

          {status === 'WAITING' && (
            <Button
              variant="outlined"
              size="large"
              color="inherit"
              onClick={() => {
                setStatus(null);
                exit();
              }}
            >
              Cancel
            </Button>
          )}
        </>
      )}
    </Styles.Main>
  );
}
