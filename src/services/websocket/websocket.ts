import * as Types from './websocket.types';

export const getWebsocket: Types.GetWebsocketService = (roomId) =>
  new Promise((resolve, reject) => {
    const wss =
      'wss://s8178.nyc1.piesocket.com/v3/{{ roomId }}?api_key=rJf6MHzR7kiWgTV4xD5bOwis0llNaa3kjpahvGqj&notify_self=1'.replace(
        /{{([\s\S]+?)roomId([\s\S]+?)}}/g,
        roomId
      );

    const websocket = new WebSocket(wss);

    websocket.addEventListener('error', (e) => reject(e), { once: true });

    websocket.addEventListener('open', () => resolve(websocket), {
      once: true,
    });
  });
