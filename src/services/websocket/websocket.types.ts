export type GetWebsocketService = (roomId: string) => Promise<WebSocket>;
