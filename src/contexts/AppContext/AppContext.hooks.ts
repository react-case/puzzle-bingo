import { create } from 'zustand';
import { generate as uuid } from 'shortid';

import { getWebsocket } from '~/services/websocket';
import * as Types from './AppContext.types';

export const useApp = create<Types.AppZustandState>((set, get) => {
  const currentRoomId = global.localStorage?.getItem('roomId') || null;

  if (currentRoomId) {
    getWebsocket(currentRoomId).then((websocket) => set({ websocket }));
  }

  return {
    roomId: currentRoomId,
    websocket: null,

    create: async () => {
      const roomId = uuid();
      const websocket = await getWebsocket(roomId);

      window.open(
        `https://social-plugins.line.me/lineit/share?url=${encodeURIComponent(
          `${location.protocol}//${location.host}?roomId=${roomId}`
        )}`
      );

      set({ roomId, websocket });

      return websocket;
    },

    join: async (roomId) => {
      const websocket = await getWebsocket(roomId);

      global.localStorage?.setItem('roomId', roomId);
      set({ roomId, websocket });

      return websocket;
    },
    exit: () => {
      const { websocket } = get();

      global.localStorage?.removeItem('roomId');
      websocket.close();

      set({ roomId: null, websocket: null });
    },
  };
});
