import { ReactNode } from 'react';

export interface AppProviderProps {
  children: ReactNode;
}

export interface AppZustandState {
  roomId?: string;
  websocket?: WebSocket;
  create: () => Promise<WebSocket>;
  join: (roomId: string) => Promise<WebSocket>;
  exit: () => void;
}
