import CssBaseline from '@mui/material/CssBaseline';
import NoSsr from '@mui/material/NoSsr';
import { ThemeProvider } from '@mui/material/styles';

import * as Types from './AppContext.types';
import * as themes from '~/themes';

export default function AppProvider({ children }: Types.AppProviderProps) {
  return (
    <NoSsr>
      <ThemeProvider theme={themes.dark}>
        <CssBaseline />

        {children}
      </ThemeProvider>
    </NoSsr>
  );
}
