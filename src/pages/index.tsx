import Head from 'next/head';
import Backdrop from '@mui/material/Backdrop';

import { GameIntro } from '~/features/app';

export default function Index() {
  return (
    <>
      <Head>
        <title>Puzzle Bingo</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Backdrop open>
        <GameIntro />
      </Backdrop>
    </>
  );
}
