import type { AppProps } from 'next/app';

import { AppMain } from '~/features/app';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <AppMain>
      <Component {...pageProps} />
    </AppMain>
  );
}
